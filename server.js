const express = require("express");
const app = express();
app.use(express.json());
app.use(
  express.urlencoded({
    extended: true,
  })
);
require("dotenv").config({
  path: ".env",
});

const PORT = 8080;
const DISCORD_TOKEN = process.env.DISCORD_TOKEN;
const TELEGRAM_TOKEN = process.env.TELEGRAM_TOKEN;
const DISCORD_CHANNEL_ID = process.env.DISCORD_CHANNEL_ID;
const TELEGRAM_CHAT_ID = process.env.TELEGRAM_CHAT_ID;

const envErrorHandler = () => {
  if (!DISCORD_CHANNEL_ID) {
    console.error("Cannot find DISCORD_CHANNEL_ID in .env file.");
  }

  if (!TELEGRAM_TOKEN) {
    console.error("Cannot find TELEGRAM_TOKEN in .env file.");
  }

  if (!DISCORD_TOKEN) {
    console.error("Cannot find DISCORD_TOKEN in .env file.");
  }

  if (!TELEGRAM_CHAT_ID) {
    console.error("Cannot find TELEGRAM_CHAT_ID in .env file.");
  }
}

if (!DISCORD_CHANNEL_ID || !DISCORD_TOKEN || !TELEGRAM_TOKEN || !TELEGRAM_CHAT_ID) {
  console.error("Please set all necessary variable in .env file. Refer to README.md for more info.");
  envErrorHandler();
  process.exit(1);
}

//setting up discord
const { Client, GatewayIntentBits } = require("discord.js");
const disClient = new Client({
  intents: [
    GatewayIntentBits.Guilds,
    GatewayIntentBits.GuildMessages,
    GatewayIntentBits.MessageContent,
  ],
});

disClient.login(DISCORD_TOKEN);

//setting up telegram bot
const { Telegraf } = require("telegraf");
const teleBot = new Telegraf(TELEGRAM_TOKEN);

//helper function to send message to discord
const sendMessageToDiscord = (message, chatID) => {
  const channel = disClient.channels.cache.get(chatID);
  if (message) channel.send(message);
};

// Steps: add bot to the group as an admin --> send a test msg
// To get the group chatID that we want to send msg to
teleBot.on("message", (ctx) => {
  const { update } = ctx;
  const { message: msg } = update;
  if (msg.chat.id != TELEGRAM_CHAT_ID) return;

  console.log("MESSAGE", msg);

  const textArr = msg.text ? msg.text.split("\n") : msg.caption.split("\n");
  const text = textArr.map((t) => `> ${t}`).join("\n");

  const message = `**${msg.from.first_name}${msg.from.last_name ? " " + msg.from.last_name : ""} (${msg.from.username}):**\n${text}`
  const messageWithDocs = `**${msg.from.first_name}${msg.from.last_name ? " " + msg.from.last_name : ""} (${msg.from.username}):**\n[Attached File(s)]\n${text}`


  if (msg.text) {
    sendMessageToDiscord(message, DISCORD_CHANNEL_ID);
    return;
  }

  if (msg.caption) {
    sendMessageToDiscord(messageWithDocs, DISCORD_CHANNEL_ID);
    return;
  }
});



teleBot.on("edited_message", (ctx) => {
  const { update } = ctx;
  const { message: msg } = update;
  if (msg.chat.id != TELEGRAM_CHAT_ID) return;

  console.log("MESSAGE", msg);

  const textArr = msg.text.split("\n");
  const text = textArr.map((t) => `> ${t}`).join("\n");

  const message = `**${msg.from.first_name}${msg.from.last_name ? " " + msg.from.last_name : ""} (${msg.from.username}):**\n_edited_\n${text}`
  const messageWithDocs = `**${msg.from.first_name}${msg.from.last_name ? " " + msg.from.last_name : ""} (${msg.from.username}):**\n_edited_\n[Attached File(s)]\n${text}`

  if (msg.text) {
    sendMessageToDiscord(message, DISCORD_CHANNEL_ID);
    return;
  }

  if (msg.caption) {
    sendMessageToDiscord(messageWithDocs, DISCORD_CHANNEL_ID);
    return;
  }
});

teleBot.launch();

app.listen(PORT, () => {
  console.log(`server has started on port ${PORT}`);
});
