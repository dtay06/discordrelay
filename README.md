# Get Started with Discord Relay

## To start

Run `nodemon server`

## To setup Discord Bot

1. Go to Discord Developer Portal
2. Create a new bot application
3. Under Bot, retrieve the token
4. Add token to `.env` file as `DISCORD_TOKEN`
5. Add channel id to `.env` file as `DISCORD_CHANNEL_ID` (see below on how to retrieve channel id)

## On Developer Mode in Discord

1. Go to settings
2. Under Advanced section, toggle on Developer Mode
   > This is to gain access to server/channel ID (right click the server/channel)

## Attaching Discord Bot to a server in discord

1. Create a new server where you are the admin
   > All the channels created under this server will be affected. Meaning if I create multiple channels and i send message from each channel, it will be directed to telegram. Need to ensure which channel should send and a way to identify this message is from which channel
2. In Discord Developer Portal, go to OAuth2, under URL generator, tick Bot with Administrator
3. Copy the generated URL and paste it on the browser
4. Add the bot to new server you created as administrator

## To setup Telegram Bot

1. In telegram, go to BotFather
2. Follow the instructions to create new Bot
3. Retrieve the token
4. Add token to `.env` file as `TELEGRAM_TOKEN`
5. Add chat id to `.env` file as `TELEGRAM_CHAT_ID` (see below on how to retrieve chat id)

## To retrieve chat id

1. Chat id are unique id of a personal or group chat. By default this bot server will fetch all message bot received in telegram. To filter the message we want to forward, we need chat id.
2. Login to [Telegram web](https://web.telegram.org)
3. Click group where you want to add Telegram bot
4. Look at the url bar, you will find something like `https://web.telegram.org/k/#<chat_id>`
5. Copy the chat id (including "-" sign) and add it to your `.env` file

## To setup the sending of messages (from telegram to discord)

1. Add the telegram Bot to a group as an admin
2. Copy the channel ID in Discord

## To setup the sending of messages (from discord to telegram) [**_curently not implemented_**]

1. Add the telegram Bot to a group as member
2. Send a test msg to retrieve the chatID
3. Replace the chatId
